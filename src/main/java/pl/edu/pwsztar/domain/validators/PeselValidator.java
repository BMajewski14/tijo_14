package pl.edu.pwsztar.domain.validators;

import org.hibernate.validator.internal.constraintvalidators.hv.pl.PESELValidator;

public class PeselValidator {

    public static boolean isValid(final String pesel){
        final int PESEL_MIN_LENGTH = 11;

        if(pesel == null || pesel.length() < PESEL_MIN_LENGTH) {
            return false;
        }

        PESELValidator peselValidator = new PESELValidator();
        peselValidator.initialize(null);

        return peselValidator.isValid(pesel,null);
    }

}
